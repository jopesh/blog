import BlockText from "./blocks/blockText"
import BlockImage from "./blocks/blockImage"
import BlockCode from "./blocks/blockCode"

export default function Blocks({ data }) {
  return data.map((item) => {
    if (item.text) {
      return <BlockText data={item} key={item.id} />
    } else if (item.image) {
      return <BlockImage data={item} key={item.id} />
    } else if (item.code) {
      return <BlockCode data={item} key={item.id} />
    } else {
      return (
        <p className="text-error">
          <strong>Fehler</strong>: Kein gültiger Block konnte gerendert werden!
        </p>
      )
    }
  })
}
