import Header from "./header"
import Footer from "./footer"

export default function Layout({ children }) {
  return (
    <div className="px-4">
      <div className="max-w-5xl mx-auto">
        <Header />
        <main>{children}</main>
        <Footer />
      </div>
    </div>
  )
}
