export default function Footer() {
  return (
    <footer className="py-12 text-xs leading-relaxed text-center transition-opacity duration-300 opacity-50 hover:opacity-100 lg:py-16">
      <p>
        Made with <span className="text-error-regular">&#9829;</span> in{" "}
        <a
          href="https://hamburg.de"
          target="_blank"
          rel="noopener noreferrer"
          className="text-foreground">
          Hamburg
        </a>
      </p>
      <p>
        Powered by{" "}
        <a
          href="https://nextjs.org"
          target="_blank"
          rel="noopener noreferrer"
          className="text-foreground">
          Next.js
        </a>{" "}
        on{" "}
        <a
          href="https://vercel.com"
          target="_blank"
          rel="noopener noreferrer"
          className="text-foreground">
          &#9650; Vercel
        </a>
      </p>
    </footer>
  )
}
