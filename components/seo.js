import Head from "next/head"

export default function SEO({
  title,
  titleSuffix,
  desc,
  og,
  ogDark,
  imageUrl,
  twitter,
  favicons,
}) {
  const ogUrl = `https://og.johnschmidt.cloud/${og}.jpeg?theme=${
    ogDark ? "dark" : "light"
  }&md=1&fontSize=100px`
  const image = imageUrl ? `${imageUrl}?fit=max&fm=jpg&w=1000` : ogUrl
  return (
    <Head>
      <title>{title + titleSuffix}</title>
      <meta name="title" content={title} />
      <meta name="description" content={desc} />
      {/* OpenGraph */}
      <meta property="og:type" content="website" />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={desc} />
      <meta property="og:image" content={image} />
      {/* Twitter */}
      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:title" content={title} />
      <meta property="twitter:description" content={desc} />
      <meta property="twitter:image" content={image} />
      <meta name="twitter:site" content={twitter} />
      {/* Favicons */}
      {favicons.map((n) => {
        const { sizes, type, rel, href } = n.attributes
        return (
          <link sizes={sizes} type={type} rel={rel} href={href} key={sizes} />
        )
      })}
    </Head>
  )
}
