import Link from "next/link"

export default function Header() {
  return (
    <header className="pt-6 text-lg font-semibold lg:pt-10">
      <div className="flex flex-col items-center justify-between sm:flex-row">
        <h1>
          <Link href="/">
            <a className="flex items-center text-foreground">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 500 500"
                className="inline-block mr-2 w-7 lg:w-8">
                <rect
                  height="500"
                  width="500"
                  style={{ fill: "var(--foreground)" }}
                />
                <path
                  d="M352.66,211.41h78.45a82.48,82.48,0,0,0,.73-9.36c.05-2.29,0-4.52-.15-6.71a68.25,68.25,0,0,0-1.12-8.5c-.14-.69-.29-1.37-.45-2.05a61.27,61.27,0,0,0-1.72-5.93c-.44-1.29-.93-2.55-1.46-3.8a57.69,57.69,0,0,0-3.68-7.17,59,59,0,0,0-4.66-6.62c-.86-1.06-1.76-2.09-2.7-3.1s-1.92-2-2.94-2.95-2.07-1.89-3.17-2.8-2.23-1.79-3.4-2.65c-.59-.43-1.19-.86-1.79-1.27q-1.83-1.26-3.76-2.43c-18-11-43.55-16.9-75.38-16.9-29.19,0-54.17,4.9-73.71,14.46L231.37,265.28h0c1.29.81,2.6,1.61,4,2.37,1,.58,2.11,1.14,3.21,1.69A99.86,99.86,0,0,0,253,275.05c1.31.41,2.63.8,4,1.18,2.71.74,5.51,1.42,8.42,2,1.45.31,2.93.59,4.43.86l41.76,7.59,3.32.67c1.58.33,3.06.66,4.43,1,.91.21,1.78.43,2.6.65,1.24.32,2.37.65,3.41,1,.69.22,1.34.45,1.94.68s1.18.46,1.71.7l.76.36a14.32,14.32,0,0,1,1.36.75l.6.39a9.71,9.71,0,0,1,1,.81,7,7,0,0,1,1.46,1.81,6,6,0,0,1,.45,1c.06.17.11.35.16.53a7.93,7.93,0,0,1,.19,1.12,10.42,10.42,0,0,1,0,1.85c-.06.38-.12.75-.2,1.13,0,.12-.07.24-.09.36s-.12.5-.19.75-.1.28-.14.43-.14.44-.22.65-.12.31-.18.46-.16.41-.25.61-.14.31-.21.46-.18.39-.29.59a5.05,5.05,0,0,1-.25.45c-.1.19-.2.38-.32.57s-.18.3-.28.45l-.35.55-.32.44-.39.53c-.11.15-.23.29-.34.43s-.28.35-.43.51-.25.28-.37.42-.3.34-.46.5l-.4.41-.49.48-.43.39-.52.46-.45.38-.55.44-.49.37-.57.43-.51.35-.61.4-.52.34c-.21.13-.42.26-.64.38l-.54.32-.67.37-.57.3c-.22.12-.45.23-.68.34l-.59.29-.71.32-.61.27-.74.3-.62.25-.76.28-.64.22-.78.26-.66.21-.8.23-.67.19-.83.21-.67.16-.86.19-.68.14-.89.16c-.22,0-.45.09-.68.12l-.93.14-.66.1-1,.11-.63.07-1.14.09-.5,0-1.66.08H302c-.43,0-.86,0-1.29,0h0l-1.76,0c-1.07,0-2.12-.07-3.13-.14a36.26,36.26,0,0,1-12.13-2.89,23.1,23.1,0,0,1-2.22-1.13,23.73,23.73,0,0,1-2-1.32,22.42,22.42,0,0,1-1.85-1.5,18.58,18.58,0,0,1-1.64-1.68q-.39-.43-.75-.9a19.33,19.33,0,0,1-1.34-2,25.17,25.17,0,0,1-3.37-12.52H226.09l-10.52,62.77,1.67,1.1c1.29.84,2.63,1.65,4,2.44,18.52,10.6,44.09,16.57,76.38,16.57a192.7,192.7,0,0,0,37.85-3.62c1.83-.36,3.64-.76,5.44-1.18s3.56-.87,5.32-1.34,3.48-1,5.19-1.5q3.85-1.19,7.56-2.55,2.46-.92,4.86-1.9c.8-.32,1.59-.66,2.38-1q2.36-1,4.64-2.12t4.5-2.28c.74-.39,1.46-.78,2.19-1.19q4.33-2.41,8.34-5.14t7.66-5.71a93.29,93.29,0,0,0,10.11-9.62q2.31-2.55,4.39-5.27,1.38-1.8,2.67-3.68a79.69,79.69,0,0,0,8.47-16q1.24-3.19,2.22-6.52a79.25,79.25,0,0,0,2.11-9.1,67.55,67.55,0,0,0,.4-19.88,51.42,51.42,0,0,0-1.19-5.93,46.83,46.83,0,0,0-3-8.25,46.28,46.28,0,0,0-2.8-5.07,48.66,48.66,0,0,0-3.44-4.73,53.88,53.88,0,0,0-4.08-4.38,56.65,56.65,0,0,0-4.71-4,62.79,62.79,0,0,0-5.34-3.69C389,237.55,375.52,232.81,359,229.76l-48.08-8.86c-17.88-3.48-20.09-7.75-19-12.65,1.74-12.34,18-20.88,35.43-20.88a35.27,35.27,0,0,1,5.4.41,31.5,31.5,0,0,1,5,1.21,26.87,26.87,0,0,1,3.39,1.42,20.6,20.6,0,0,1,9.67,9.75c.19.4.36.81.52,1.23a23.19,23.19,0,0,1,1.34,10Z"
                  style={{ fill: "var(--background)" }}
                />
                <path
                  d="M174.72,101.33a32.08,32.08,0,0,0,3.39,4.34,30.88,30.88,0,0,0,2.66,2.58,33.65,33.65,0,0,0,3,2.31,36.67,36.67,0,0,0,6.76,3.7,37.84,37.84,0,0,0,3.75,1.35,43.24,43.24,0,0,0,12.52,1.81q1.13,0,2.22-.06c1.48-.07,2.95-.2,4.4-.41a49.84,49.84,0,0,0,7.08-1.53c.95-.28,1.89-.58,2.82-.91a53.54,53.54,0,0,0,10.47-5.07,52.29,52.29,0,0,0,6.67-4.94l.39-.34c1-.91,2-1.85,3-2.83s1.93-2.09,2.82-3.19c.77-.94,1.49-1.9,2.17-2.89l.76-1.11A45.06,45.06,0,0,0,252.46,89a41.56,41.56,0,0,0,2.2-5.42,40,40,0,0,0,1.08-4c.15-.67.27-1.35.38-2,3.48-22.15-12.65-39.86-36.38-39.86s-45.86,17.71-49.66,39.86c-.12.68-.22,1.36-.3,2a38.82,38.82,0,0,0-.24,4,32.36,32.36,0,0,0,3.36,14.55A34.55,34.55,0,0,0,174.72,101.33Z"
                  style={{ fill: "var(--background)" }}
                />
                <path
                  d="M221.85,291.82l4.05-24.17.92-5.51h0l19.36-115.56,1-6.2L249.13,129h-87.3L120.39,378.22C118,392.14,110.9,398.47,90,398.47l-2.4,0c-1.57-.05-3.19-.14-4.87-.29-.93-.09-1.88-.19-2.85-.31h0L69.14,462.37c4,.29,7.42.47,10.77.55,1.34,0,2.66.06,4,.07h2c19.06,0,36.54-1.69,52-5.73q2.43-.63,4.8-1.35l2.35-.74c1.56-.5,3.09-1,4.6-1.6.75-.28,1.5-.56,2.24-.86,1.48-.59,2.94-1.2,4.38-1.85,2.15-1,4.24-2,6.28-3.11,1.36-.73,2.7-1.5,4-2.3a82.87,82.87,0,0,0,9.31-6.57c.73-.6,1.46-1.21,2.17-1.83a82,82,0,0,0,6.11-6c1-1.13,2-2.29,3-3.49a84,84,0,0,0,10.26-16.15q1.65-3.37,3.08-7c.64-1.62,1.24-3.27,1.81-5s1.12-3.43,1.62-5.21a130.77,130.77,0,0,0,3.16-14.14l3.06-18.27.94-5.58h0l10-59.45Z"
                  style={{ fill: "var(--background)" }}
                />
              </svg>
              <span>johnschmidt.de</span>
            </a>
          </Link>
        </h1>
        <div>
          <a
            href="https://www.obama.org/anguish-and-action/"
            target="_blank"
            rel="noopener noreferrer"
            className="flex items-center mt-2 text-warn-dark dark:text-warn-regular sm:mt-0">
            <span>#BlackLivesMatter</span>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="w-4 ml-1"
              viewBox="0 0 24 24"
              width="24"
              height="24">
              <path fill="none" d="M0 0h24v24H0z" />
              <path
                fill="currentColor"
                d="M10 6v2H5v11h11v-5h2v6a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V7a1 1 0 0 1 1-1h6zm11-3v9l-3.794-3.793-5.999 6-1.414-1.414 5.999-6L12 3h9z"
              />
            </svg>
          </a>
        </div>
      </div>
    </header>
  )
}
