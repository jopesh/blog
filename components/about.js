import { Image } from "react-datocms"

export default function About({ data }) {
  return (
    <section className="max-w-lg lg:max-w-none lg:grid lg:grid-cols-2 lg:gap-12">
      <div className="flex items-start align-top">
        <div className="flex-shrink-0 overflow-hidden rounded-full shadow-small">
          <Image
            data={data.author.avatar.image}
            className="overflow-hidden rounded-full"
            fadeInDuration="0"
          />
        </div>
        <div
          className="w-auto pl-4 text-sm"
          dangerouslySetInnerHTML={{ __html: data.author.bio }}
        />
      </div>
    </section>
  )
}
