import Link from "next/link"
import { Image } from "react-datocms"

export default function FeaturedPost({ data }) {
  const { slug, title, thumbnail, date, content, fromNow } = data
  return (
    <section>
      {thumbnail && (
        <Link href="/post/[slug]" as={`/post/${slug}`}>
          <a aria-label={title}>
            <Image
              data={thumbnail.big}
              className="-mx-4 lg:mx-0 lg:shadow-md"
            />
          </a>
        </Link>
      )}
      <div className="mt-4 lg:grid lg:grid-cols-2 lg:gap-10 lg:mt-8">
        <div className="mb-4 lg:mb-0">
          <Link href="/post/[slug]" as={`/post/${slug}`}>
            <a className="text-foreground">
              <h2 className="text-3xl font-medium leading-snug md:text-4xl lg:text-5xl lg:leading-tight sm:text-4xl">
                {title}
              </h2>
            </a>
          </Link>
          <time
            dateTime={date}
            className="inline-block lg:text-lg lg:mt-2 text-accents-600">
            {fromNow}
          </time>
        </div>
        <div>
          <div dangerouslySetInnerHTML={{ __html: content[0].excerptLong }} />
          <Link href="/post/[slug]" as={`/post/${slug}`}>
            <a className="inline-block mt-4">Lesen →</a>
          </Link>
        </div>
      </div>
    </section>
  )
}
