export function Spacer() {
  return <div className={`block h-6 md:h-8 lg:h-10`} />
}

export function AuthorCredit({ data }) {
  const AuthorText = () => (
    <span>
      <span>{data.author}</span>
      {data.copyright && <span> auf {data.copyright}</span>}
    </span>
  )
  return (
    <p className="my-2 text-sm text-center text-accents-500">
      <span>Foto von </span>
      {data.customData.authorUrl ? (
        <a
          href={data.customData.authorUrl}
          target="_blank"
          rel="noopener noreferrer"
          className="text-accents-700">
          <AuthorText />
        </a>
      ) : (
        <AuthorText />
      )}
    </p>
  )
}
