import { Image } from "react-datocms"

export default function BlockImage({ data }) {
  return (
    <div className="my-8 lg:my-12">
      <Image data={data.image.responsiveImage} className="shadow-small -mx-4 md:mx-0" />
      {data.imageDesc && (
        <p className="w-full my-2 text-sm text-center text-accents-500">
          {data.imageDesc}
        </p>
      )}
    </div>
  )
}
