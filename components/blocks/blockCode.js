import CopyCode from "./copyCode"

class BlockCode extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      copied: false,
    }
  }
  copiedState() {
    this.setState({ copied: true })
    setTimeout(() => {
      this.setState({ copied: false })
    }, 4000)
  }
  render() {
    const data = this.props.data
    return (
      <div className="relative max-w-4xl my-6 -mx-4 lg:my-12 sm:mx-auto">
        <pre
          className={`language-${data.codeLang} text-sm md:text-base shadow-small`}>
          <code
            dangerouslySetInnerHTML={{
              __html: data.codeHigh,
            }}
          />
        </pre>
        {data.codeDesc && (
          <div className="w-full my-2 text-sm text-center text-accents-500">
            <p>{data.codeDesc}</p>
          </div>
        )}
        <CopyCode
          text={this.props.data.code}
          copied={this.state.copied}
          onCopy={() => this.copiedState()}
        />
      </div>
    )
  }
}

export default BlockCode
