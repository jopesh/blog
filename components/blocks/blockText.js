import React from "react"
import { getReact } from "../../lib/utils"

const components = {
  p: (props) => <p className="mb-6 ">{props.children}</p>,
  h1: (props) => <h1 className="mb-4 text-4xl font-bold">{props.children}</h1>,
  h2: (props) => <h2 className="mb-4 text-3xl font-bold">{props.children}</h2>,
  h3: (props) => <h3 className="mb-4 text-2xl font-bold">{props.children}</h3>,
  ol: (props) => <ol className="pl-4 mb-6 ml-6 list-disc">{props.children}</ol>,
  ul: (props) => (
    <ul className="pl-4 mb-6 ml-6 list-decimal">{props.children}</ul>
  ),
  li: (props) => <li className="pl-2">{props.children}</li>,
  blockquote: (props) => (
    <blockquote className="pl-4 italic border-l-4 border-success-regular">
      {props.children}
    </blockquote>
  ),
  wrapper: (props) => (
    <div className="max-w-2xl mx-auto lg:max-w-3xl lg:text-lg xl:text-xl xl:max-w-4xl">
      {props.children}
    </div>
  ),
}

export default function BlockText({ data }) {
  return getReact(
    data.text,
    components,
    components.wrapper,
    React.createElement
  )
}
