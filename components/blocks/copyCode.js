import { CopyToClipboard } from "react-copy-to-clipboard"

export default function CopyCode({ copied, text, onCopy }) {
  return (
    <CopyToClipboard text={text} onCopy={() => onCopy()}>
      <button
        className={`text-xs absolute top-0 right-0 m-4 p-1 rounded hidden md:block transition-colors duration-200 ease bg-none border ${
          copied
            ? "bg-success-regular text-white border-success-regular"
            : "text-white border-white hover:bg-white hover:text-black hover:border-white"
        }`}>
        <svg
          className="inline-block w-4"
          fill="none"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          viewBox="0 0 24 24"
          stroke="currentColor">
          {copied ? (
            <path d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-6 9l2 2 4-4"></path>
          ) : (
            <path d="M8 5H6a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2v-1M8 5a2 2 0 002 2h2a2 2 0 002-2M8 5a2 2 0 012-2h2a2 2 0 012 2m0 0h2a2 2 0 012 2v3m2 4H10m0 0l3-3m-3 3l3 3"></path>
          )}
        </svg>
        {copied ? " Ist kopiert!" : " Code kopieren"}
      </button>
    </CopyToClipboard>
  )
}
