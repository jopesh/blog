import Blocks from "./blocks"
import { AuthorCredit } from "./helpers"
import { Image } from "react-datocms"

export default function Post({ data }) {
  const { title, thumbnail, date, dateString, content } = data
  return (
    <article className="my-6 md:my-10 lg:my-12">
      <section className={`text-center ${thumbnail && "lg:text-left"}`}>
        <time
          dateTime={date}
          className="block text-lg lg:text-xl text-accents-600">
          {dateString}
        </time>
        <h1 className="mt-2 text-4xl font-bold leading-tight md:text-5xl lg:text-6xl">
          {title}
        </h1>
        {thumbnail && (
          <Image
            className="mt-4 mb-2 -mx-4 lg:mt-8 lg:mx-0 lg:shadow-md"
            data={thumbnail.responsiveImage}
          />
        )}
        {thumbnail && thumbnail.author && <AuthorCredit data={thumbnail} />}
      </section>
      <section className="my-6 lg:my-12">
        <Blocks data={content} />
      </section>
    </article>
  )
}
