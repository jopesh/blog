import Link from "next/link"
import { Spacer } from "./helpers"

export default function MorePosts({ data }) {
  return (
    <section>
      <Spacer />
      <Spacer />
      <ul className="grid grid-cols-1 gap-10 list-none md:grid-cols-2">
        {data.map((post) => {
          return (
            <li key={post.id}>
              <Link href="/post/[slug]" as={`/post/${post.slug}`}>
                <a className="text-foreground">
                  <h2 className="text-2xl font-medium leading-snug lg:text-3xl xl:text-4xl">
                    {post.title}
                  </h2>
                </a>
              </Link>
              <time
                dateTime={post.date}
                className="block mb-4 text-sm lg:text-base text-accents-600">
                {post.fromNow}
              </time>
              <div
                dangerouslySetInnerHTML={{ __html: post.content[0].excerpt }}
              />
              <Link href="/post/[slug]" as={`/post/${post.slug}`}>
                <a className="inline-block mt-4">Lesen →</a>
              </Link>
            </li>
          )
        })}
      </ul>
    </section>
  )
}
