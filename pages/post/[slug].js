import Layout from "../../components/layout"
import Post from "../../components/post"
import SEO from "../../components/seo"

import { getSlugs, getPostData, getGlobalSeo } from "../../lib/api"

export default function PostPage({ postData, globalSeo }) {
  const { title } = postData
  const seo = postData.seo
  const imageUrl = seo && seo.image && seo.image.url
  const favicons = globalSeo.site.faviconMetaTags
  return (
    <Layout>
      <SEO
        title={title}
        titleSuffix={globalSeo.site.global.titleSuffix}
        desc={
          (seo && seo.description) ||
          globalSeo.site.global.fallbackSeo.description
        }
        og={title}
        imageUrl={imageUrl}
        favicons={favicons}
      />
      <Post data={postData} />
    </Layout>
  )
}

export async function getStaticProps({ params }) {
  const globalSeo = await getGlobalSeo()
  const postData = await getPostData(params.slug)
  return {
    props: {
      postData,
      globalSeo,
    },
  }
}

export async function getStaticPaths() {
  const paths = await getSlugs()
  return {
    paths,
    fallback: false,
  }
}
