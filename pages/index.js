import Layout from "../components/layout"
import About from "../components/about"
import FeaturedPost from "../components/featured"
import MorePosts from "../components/more"
import SEO from "../components/seo"

import { Spacer } from "../components/helpers"
import { getGlobalSeo, getAllPosts, getAboutMeData } from "../lib/api"

export default function Overview({ allPostData, globalSeo, aboutMeData }) {
  const featured = allPostData.allPosts[0]
  const more = allPostData.allPosts.slice(1)
  const { faviconMetaTags, global } = globalSeo.site
  return (
    <Layout>
      <SEO
        title={global.siteName}
        titleSuffix=""
        desc={global.fallbackSeo.description}
        twitter={global.twitterAccount}
        og={`**${global.siteName}**`}
        ogDark={false}
        favicons={faviconMetaTags}
      />
      <Spacer />
      <About data={aboutMeData} />
      <Spacer />
      <FeaturedPost data={featured} />
      {more.length > 0 && <MorePosts data={more} />}
    </Layout>
  )
}

export async function getStaticProps() {
  const globalSeo = await getGlobalSeo()
  const allPostData = await getAllPosts()
  const aboutMeData = await getAboutMeData()
  return {
    props: { allPostData, globalSeo, aboutMeData },
  }
}
