const defaultTheme = require("tailwindcss/defaultTheme")

module.exports = {
  purge: ["./components/**/*.js", "./pages/**/*.js"],
  theme: {
    colors: {
      foreground: "var(--foreground)",
      background: "var(--background)",
      primary: "var(--primary)",
      white: "#ffffff",
      black: "#000000",
      error: {
        light: "var(--error-light)",
        regular: "var(--error)",
        dark: "var(--error-dark)",
      },
      success: {
        light: "var(--success-light)",
        regular: "var(--success)",
        dark: "var(--success-dark)",
      },
      warn: {
        light: "var(--warning-light)",
        regular: "var(--warning)",
        dark: "var(--warning-dark)",
      },
      accents: {
        "100": "var(--accents-1)",
        "200": "var(--accents-2)",
        "300": "var(--accents-3)",
        "400": "var(--accents-4)",
        "500": "var(--accents-5)",
        "600": "var(--accents-6)",
        "700": "var(--accents-7)",
        "800": "var(--accents-8)",
      },
    },
    extend: {
      fontFamily: {
        sans: ["Inter var", "Inter", ...defaultTheme.fontFamily.sans],
      },
      screens: {
        dark: { raw: "(prefers-color-scheme: dark)" },
      },
    },
  },
  variants: {},
  plugins: [],
}
