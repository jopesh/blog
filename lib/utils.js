import unified from "unified"
import markdown from "remark-parse"
import remark2rehype from "remark-rehype"
import remark2react from "remark-react"
import html from "rehype-stringify"
import slug from "remark-slug"
import smarty from "@silvenon/remark-smartypants"

export function getExcerptHtml(string, length) {
  const cut = string.substr(0, string.lastIndexOf(" ", length)) + "..."
  return unified()
    .use(markdown)
    .use(smarty)
    .use(remark2rehype)
    .use(html)
    .processSync(cut).contents
}

export function getParsedHtml(string) {
  return unified()
    .use(markdown)
    .use(slug)
    .use(smarty)
    .use(remark2rehype)
    .use(html)
    .processSync(string).contents
}

export function getReact(string, components, wrapper, createElement) {
  return unified()
    .use(markdown)
    .use(remark2react, {
      createElement: createElement,
      remarkReactComponents: components,
      fragment: wrapper,
      sanitize: true,
    })
    .processSync(string).result
}
