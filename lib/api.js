import { GraphQLClient } from "graphql-request"
import { getExcerptHtml, getParsedHtml } from "./utils"
import moment from "moment"
moment.locale("de")
import Prism from "prismjs"

// Basic GraphQL query function with authentication
export function datoRequest({ query, variables, preview }) {
  const endpoint = preview
    ? "https://graphql.datocms.com/preview"
    : "https://graphql.datocms.com"
  const client = new GraphQLClient(endpoint, {
    headers: {
      authorization: `Bearer ${process.env.NEXT_DATOCMS_API_TOKEN}`,
    },
  })
  return client.request(query, variables)
}

// Fragment for responsiveImage GraphQL queries
const responsiveImageFragment = /* GraphQL */ `
  fragment responsiveImageFragment on ResponsiveImage {
    srcSet
    webpSrcSet
    sizes
    src
    width
    height
    aspectRatio
    alt
    title
    base64
  }
`

// GraphQL query function for global SEO data
export async function getGlobalSeo() {
  const req = /* GraphQL */ `
    {
      site: _site {
        faviconMetaTags {
          attributes
          tag
          content
        }
        global: globalSeo {
          fallbackSeo {
            description
            title
            title
            twitterCard
          }
          siteName
          titleSuffix
          twitterAccount
        }
      }
    }
  `
  return await datoRequest({
    query: req,
  })
}

// GraphQL query for all of the Posts data, mainly for index list
export async function getAllPosts() {
  const req = /* GraphQL */ `
    {
      allPosts(orderBy: date_DESC) {
        id
        title
        slug
        date
        content {
          ... on TextRecord {
            text(markdown: false)
          }
        }
        thumbnail {
          big: responsiveImage(
            imgixParams: { fit: crop, w: 1024, h: 512, auto: format }
          ) {
            ...responsiveImageFragment
          }
        }
      }
    }
    ${responsiveImageFragment}
  `
  const res = await datoRequest({
    query: req,
  })

  const updatedPosts = res.allPosts.map((n) => {
    const firstBlock = n.content[0]
    firstBlock.excerpt = getExcerptHtml(firstBlock.text, 200)
    firstBlock.excerptLong = getExcerptHtml(firstBlock.text, 300)
    n.fromNow = moment(n.date).fromNow()
    return n
  })

  return {
    allPosts: updatedPosts,
  }
}

// GraphQL query for About me section
export async function getAboutMeData() {
  const req = /* GraphQL */ `
    {
      author {
        avatar {
          image: responsiveImage(
            imgixParams: {
              w: 64
              h: 64
              fit: crop
              mask: "ellipse"
              auto: format
            }
          ) {
            ...responsiveImageFragment
          }
        }
        bio
      }
    }
    ${responsiveImageFragment}
  `
  return await datoRequest({
    query: req,
  })
}

// GraphQL query and transformation of the slug-data to be used by getStaticPaths
export async function getSlugs() {
  const req = /* GraphQL */ `
    {
      allPosts {
        slug
      }
    }
  `
  const data = await datoRequest({
    query: req,
  })
  return data.allPosts.map((n) => {
    return {
      params: {
        slug: n.slug,
      },
    }
  })
}

// GraphQL query for single post data with slug filter
export async function getPostData(slug) {
  const req = /* GraphQL */ `
    query postData($slug: String!) {
      post(filter: { slug: { eq: $slug } }) {
        id
        slug
        title
        date
        thumbnail {
          title
          alt
          author
          customData
          copyright
          responsiveImage(
            imgixParams: { fit: crop, w: 1024, h: 512, auto: format }
          ) {
            ...responsiveImageFragment
          }
        }
        seo {
          description
          title
          image {
            url
          }
        }
        content {
          ... on TextRecord {
            id
            text(markdown: false)
          }
          ... on ImageRecord {
            id
            imageDesc
            image {
              id
              author
              copyright
              customData
              responsiveImage(
                imgixParams: { fit: crop, w: 1024, auto: format }
              ) {
                ...responsiveImageFragment
              }
            }
          }
          ... on CodeRecord {
            id
            code
            codeLang
            codeDesc
          }
        }
      }
    }
    ${responsiveImageFragment}
  `
  const res = await datoRequest({
    query: req,
    variables: {
      slug: slug,
    },
  })
  // Iterate over the posts content array and change stuff
  const updatedContent = res.post.content.map((n) => {
    if (n.code) {
      const h = Prism.highlight(n.code, Prism.languages[n.codeLang], n.codeLang)
      n.codeHigh = h
    } else if (n.textBody) {
      // Parse post body with remark
      const t = getParsedHtml(n.textBody)
      n.textBodyHtml = t
    }
    // Return the modified object
    return n
  })

  // add a prop "dateString"
  const dateString = moment(res.post.date).format("dddd, D. MMMM YYYY")

  return {
    content: updatedContent,
    dateString: dateString,
    ...res.post,
  }
}
